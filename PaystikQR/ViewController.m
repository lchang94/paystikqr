//
//  ViewController.m
//  PaystikQR
//
//  Created by Luke Chang on 5/14/13.
//  Copyright (c) 2013 Luke Chang. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize scannedImage, parsedPayload;

//handler for user initiating scan
- (IBAction) didPressScan
{
    //create and present code reader
    ZBarReaderController* reader = [ZBarReaderController new];
    reader.readerDelegate = self;
    if([ZBarReaderController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
        reader.sourceType = UIImagePickerControllerSourceTypeCamera;

    [self presentModalViewController: reader
                            animated: YES];
    [reader release];
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSMutableArray* results =[info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol* symbol = [results objectAtIndex:0];

    
    parsedPayload.text = symbol.data;
    scannedImage.image = [info objectForKey: UIImagePickerControllerOriginalImage];
    
    [picker dismissModalViewControllerAnimated: YES];
     
}

//validates if scanned QR is a valid Paystik code
- (IBAction) validatePayload
{
    /*assumes that paystik recognizes payments by checking URL base = https://debit.io,
    since it rediects to checkout.paystik.com*/
    NSError* error = NULL;
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern: @"https://debit.io/.+" options:NSRegularExpressionCaseInsensitive error: &error];
    NSUInteger matches = [regex numberOfMatchesInString: parsedPayload.text
                                                options: 0
                                                  range: NSMakeRange(0, [parsedPayload.text length])];
    if (error != nil)
    {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle: @"Error"
                                                        message: @"An error occurred validating the code payload. We apologize for the inconvenience."
                                                       delegate: nil
                                              cancelButtonTitle: @"Dismiss"
                                              otherButtonTitles: nil];
        [alert show];
        [alert release];
    }
    else if (matches == 0)
    {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle: @"Validation Complete"
                                                        message: @"This is not a valid Paystik QR code."
                                                       delegate: nil
                                              cancelButtonTitle: @"Dismiss"
                                              otherButtonTitles: nil];
        [alert show];
        [alert release];

    }
    else
    {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle: @"Validation Complete"
                                                        message: @"This is a valid Paystik QR code."
                                                       delegate: nil
                                              cancelButtonTitle: @"Dismiss"
                                              otherButtonTitles: nil];
        [alert show];
        [alert release];
    }
    
    
}

//cleanup
- (void) dealloc
{
    self.scannedImage = nil;
    self.parsedPayload = nil;
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [parsedPayload setEditable: NO];

    for (id object in [self.view subviews]) {
        if ([object isKindOfClass:[UIButton class]]) {
            //set custom buttons
            [object setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
            [object setTitleColor: [UIColor colorWithRed:255/255.0f green:253/255.0f blue:166/255.0f alpha:1.0f] forState: UIControlStateHighlighted];
            [object setBackgroundColor: [UIColor colorWithRed:255/255.0f green:127/255.0f blue:18/255.0f alpha:1.0f]];
            [object layer].cornerRadius = 7;  
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
