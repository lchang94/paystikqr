//
//  ViewController.h
//  PaystikQR
//
//  Created by Luke Chang on 5/14/13.
//  Copyright (c) 2013 Luke Chang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBarSDK.h"
#import <QuartzCore/QuartzCore.h>

@interface ViewController : UIViewController
    <ZBarReaderDelegate>
{
    UIImageView *scannedImage;
    UITextView *parsedPayload;
}

@property (nonatomic, retain) IBOutlet UIImageView *scannedImage;
@property (nonatomic, retain) IBOutlet UITextView *parsedPayload;

- (IBAction) didPressScan;
- (IBAction) validatePayload;

@end
